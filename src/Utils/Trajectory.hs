module Utils.Trajectory
  ( Segment(..)
  , project
  , Ann(..)
  , inspect
  , Inspection(..)
  , pull
  , pullV
  ) where

import RIO.Local hiding (force)

import Data.List (unfoldr)
import Geomancy.Vec3 qualified as Vec3

data Segment = Segment
  { sIx     :: Int   -- Step index
  , sPos    :: Vec3  -- Step position
  , sVel    :: Vec3  -- Projected velocity vector
  , sNorm   :: Float -- Scalar velocity
  , sDecr   :: Bool  -- Scalar velocity decreased?
  , sAnn    :: Ann
  }
  deriving (Eq, Ord, Show)

instance NFData Segment where
  rnf seg = seg `seq` ()

data Ann
  = Fall      -- ^ Unpowered orbit
  | Terminal  -- ^ Capture or something
  | Apoapsis  -- ^ Local minimum
  | Periapsis -- ^ Local maximum
  deriving (Eq, Ord, Show)

project :: Float -> [(Float, Vec3)] -> (Vec3, Vec3) -> [Segment]
project dt gs (startPos, startVel) = unfoldr_ nextSegment initialSegment
  where
    -- XXX: massage step function into being both accumulator and result.
    unfoldr_ f = unfoldr $ fmap (\a -> (a, a)) . f

    initialSegment = Segment
      { sIx     = 0
      , sPos    = startPos
      , sVel    = startVel
      , sNorm   = sqrt $ Vec3.dot startVel startVel
      , sDecr   = False
      , sAnn    = Fall
      }

    nextSegment Segment{..} =
      if sAnn == Terminal then
        Nothing
      else
        case step sPos sVel of
          Nothing ->
            Just Segment
              { sIx  = sIx + 1
              , sAnn = Terminal
              , ..
              }
          Just vel' ->
            Just Segment
              { sIx     = sIx + 1
              , sAnn    = ann'
              , sPos    = pos'
              , sVel    = vel'
              , sNorm   = vel_
              , sDecr   = decr'
              }
            where
              pos' = sPos + vel' ^* dt
              vel_ = sqrt $ Vec3.dot vel' vel'
              decr' = vel_ < sNorm
              ann' =
                case (sDecr, decr') of
                  _ | sIx < 2 ->
                    Fall
                  (True, False) ->
                    Apoapsis
                  (False, True) ->
                    Periapsis
                  _ ->
                    Fall

    step massPos intialMoment = foldr stepF (pure intialMoment) gs
      where
        stepF (gravMass, gravPos) accM = do
          let
            force = pull gravMass gravPos massPos
            forceV = Vec3.normalize (gravPos - massPos) ^* force
          guard $ force < 8.0
          acc <- accM
          pure $ acc + forceV

pullV :: Float -> Vec3 -> Vec3 -> Vec3
pullV gm gp mp =
  Vec3.normalize (gp - mp) ^* pull gm gp mp
{-# INLINE pullV #-}

pull :: Float -> Vec3 -> Vec3 -> Float
pull gm gp mp = gm / Vec3.dot q q
  where
    q = gp - mp
{-# INLINE pull #-}

data Inspection = Inspection
  { apsides :: [Segment]
  , entry   :: Maybe Segment
  }

inspect :: [Segment] -> Inspection
inspect segments = Inspection{..}
  where
    apsides = do
      seg@Segment{sAnn} <- segments
      guard (sAnn == Apoapsis || sAnn == Periapsis)
      pure seg

    entry = case dropWhile notTerminal segments of
      seg@Segment{sAnn=Terminal} : _rest ->
        Just seg
      _ ->
        Nothing

    notTerminal Segment{sAnn} = sAnn /= Terminal

-- unloop :: [(Segment, a, b)] -> [(Segment, a, b)]
-- unloop = \case
--   [] -> []
--   [x] -> [x]
--   (start, _a, _b) : rest ->
--     takeWhile (not looped) rest
