-- TODO: upstream to Engine.Events

module Utils.Sink where

import RIO

import Engine.Events (Sink(..))
import Engine.Types (StageRIO)

class Monad m => MonadSink e st m | m -> e, m -> st where
  getSink :: m (Maybe (Sink e st))

  sendSinkSignal :: StageRIO st () -> m ()
