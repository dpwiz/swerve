module Global.Render
  ( Stage
  , Frame
  , StageFrameRIO
  , component
  , RenderPasses(..)
  , allocateRenderpasses
  , Pipelines(..)
  , allocatePipelines
  , getSceneLayout
  , getSunLayout
  ) where

import RIO

import Control.Monad.Trans.Resource (ResourceT)
import Data.Tagged (Tagged(..))
import Vulkan.Core10 qualified as Vk
import Foreign (nullPtr)
import Engine.Stage.Component qualified as Stage
import Engine.Types (StageRIO)
import Engine.Types qualified as Engine
import Engine.Vulkan.Swapchain qualified as Swapchain
import Engine.Vulkan.Types (HasSwapchain, RenderPass(..))
import Render.Basic qualified as Basic
import Render.DescSets.Set0 (Scene)
import Render.DescSets.Set0 qualified as Scene
import Render.DescSets.Sun (Sun)
import Render.ImGui qualified as ImGui
import DearImGui.Raw.IO qualified
import Render.Samplers qualified as Samplers

import Global.Resource.Combined qualified as Combined
import Global.Resource.CubeMap qualified as CubeMap

type Stage = Engine.Stage RenderPasses Pipelines
type Frame = Engine.Frame RenderPasses Pipelines
type StageFrameRIO fr rs = Engine.StageFrameRIO RenderPasses Pipelines fr rs

component :: Stage.Rendering RenderPasses Pipelines st
component = Stage.Rendering
  { rAllocateRP = allocateRenderpasses
  , rAllocateP = allocatePipelines
  }

data RenderPasses = RenderPasses
  { rpBasic :: Basic.RenderPasses
  }

instance RenderPass RenderPasses where
  updateRenderpass swapchain RenderPasses{..} = RenderPasses
    <$> updateRenderpass swapchain rpBasic

  refcountRenderpass RenderPasses{..} = do
    refcountRenderpass rpBasic

allocateRenderpasses
  :: HasSwapchain swapchain
  => swapchain
  -> ResourceT (StageRIO st) RenderPasses
allocateRenderpasses swapchain = do
  rpBasic <- Basic.allocate_ swapchain
  pure RenderPasses{..}

data Pipelines = Pipelines
  { pBasic :: Basic.Pipelines
  }

allocatePipelines
  :: HasSwapchain swapchain
  => swapchain
  -> RenderPasses
  -> ResourceT (StageRIO st) Pipelines
allocatePipelines swapchain RenderPasses{..} = do
  samplers <- Samplers.allocate (Swapchain.getAnisotropy swapchain)
  let
    -- XXX: match with Stage.Example.Render.initialData ⚠️
    sceneBinds =
      Scene.mkBindings
        samplers
        Combined.sources
        CubeMap.sources
        0 -- no shadows

  pBasic <- Basic.allocatePipelines
    sceneBinds
    (Swapchain.getMultisample swapchain)
    rpBasic

  void $! ImGui.allocate swapchain (Basic.rpForwardMsaa rpBasic) 0
  DearImGui.Raw.IO.setIniFilename nullPtr

  pure Pipelines{..}

getSceneLayout :: Pipelines -> Tagged '[Scene] Vk.DescriptorSetLayout
getSceneLayout = Basic.getSceneLayout . pBasic

getSunLayout :: Pipelines -> Tagged '[Sun] Vk.DescriptorSetLayout
getSunLayout = Basic.getSunLayout . pBasic
