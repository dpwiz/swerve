{-# LANGUAGE DeriveAnyClass #-}

-- {-# OPTIONS_GHC -fforce-recomp #-}

module Global.Resource.Sound
  ( Collection(..)
  , Sources
  , configs
  , indices
  ) where

import RIO

import Data.Distributive (Distributive(..))
import Data.Distributive.Generic (genericCollect)
import Data.Functor.Rep (Co(..), Representable)
import GHC.Generics (Generic1)
import Resource.Collection (enumerate)
import Resource.Opus qualified as Opus
import Resource.Source qualified as Source
import Resource.Static qualified as Static

data Collection a = Collection
  { crash         :: a
  , crash_alert   :: a
  , crash_averted :: a

  , warp_in  :: a
  , warp_out :: a

  , item_boost :: a
  , item_shed  :: a
  , item_jump  :: a

  , thrust :: a
  , bg     :: a
  }
  deriving stock (Show, Functor, Foldable, Traversable, Generic, Generic1)
  deriving Applicative via (Co Collection)
  deriving anyclass (Representable)

instance Distributive Collection where
  collect = genericCollect

type Sources = Collection Opus.Source

Static.filePatterns Static.Files "data/sound"

configs :: Collection Opus.Config
configs = Collection
  { crash = Opus.Config
      { gain        = 1.0
      , loopingMode = False
      , byteSource  = Source.File Nothing CRASH_OPUS
      }
  , crash_alert = Opus.Config
      { gain        = 1.0
      , loopingMode = False
      , byteSource  = Source.File Nothing CRASH_ALERT_OPUS
      }
  , crash_averted = Opus.Config
      { gain        = 1.0
      , loopingMode = False
      , byteSource  = Source.File Nothing CRASH_AVERTED_OPUS
      }

  , item_boost = Opus.Config
      { gain        = 1.0
      , loopingMode = False
      , byteSource  = Source.File Nothing ITEM_BOOST_OPUS
      }
  , item_shed = Opus.Config
      { gain        = 1.0
      , loopingMode = False
      , byteSource  = Source.File Nothing ITEM_SHED_OPUS
      }
  , item_jump = Opus.Config
      { gain        = 1.0
      , loopingMode = False
      , byteSource  = Source.File Nothing ITEM_JUMP_OPUS
      }

  , warp_in = Opus.Config
      { gain        = 1.25
      , loopingMode = False
      , byteSource  = Source.File Nothing WARP_IN_OPUS
      }
  , warp_out = Opus.Config
      { gain        = 1.0
      , loopingMode = False
      , byteSource  = Source.File Nothing WARP_OUT_OPUS
      }

  , thrust = Opus.Config
      { gain        = 1.0
      , loopingMode = True
      , byteSource  = Source.File Nothing THRUST_OPUS
      }

  , bg = Opus.Config
      { gain        = 0.25
      , loopingMode = True
      , byteSource  = Source.File Nothing BG_OPUS
      }
  }

indices :: Collection Int
indices = fmap fst $ enumerate configs
