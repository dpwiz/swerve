{-# LANGUAGE OverloadedLists #-}

module Global.Resource.Assets
  ( Assets(..)
  , load
  ) where

import RIO

import Control.Monad.Trans.Resource qualified as Resource
import Geomancy (vec2, vec3)
import Geomancy.Vec3 qualified as Vec3
import Geomancy.Vec4 qualified as Vec4
import UnliftIO.Resource (MonadResource)
import Vulkan.Core10 qualified as Vk
import Engine.Sound.Device qualified as SoundDevice
import Engine.Sound.Source qualified as SoundSource

import Engine.Vulkan.Types (MonadVulkan, Queues)
import Geometry.Cube qualified as Cube
import Geometry.Quad qualified as Quad
import Geometry.Icosphere qualified as Icosphere
import Render.Lit.Colored.Model qualified as LitColored
import Render.Lit.Material.Collect qualified as Collect
import Resource.Buffer qualified as Buffer
import Resource.Combined.Textures qualified as CombinedTextures
import Resource.CommandBuffer (withPools)
import Resource.Font qualified as Font
import Resource.Mesh.Codec qualified as Mesh
import Resource.Model qualified as Model
import Resource.Texture.Ktx2 qualified as Ktx2
import Resource.Opus qualified as Opus

import Global.Resource.Combined qualified as Combined
import Global.Resource.CubeMap qualified as CubeMap
import Global.Resource.Font (FontCollection)
import Global.Resource.Font qualified as GameFont
import Global.Resource.Material (MaterialCollection)
import Global.Resource.Model qualified as GameModel
import Global.Resource.Sound qualified as Sound
import Global.Resource.Texture qualified as Textures

data Assets = Assets
  { aKey       :: Resource.ReleaseKey
  , aFonts     :: FontCollection
  , aModels    :: GameModel.Collection
  , aTextures  :: Combined.Textures
  , aCubeMaps  :: CubeMap.Textures
  , aMaterials :: MaterialCollection
  , aSounds    :: Sound.Collection Opus.Source
  }

load
  :: ( MonadVulkan env m
     , HasLogFunc env
     , MonadThrow m
     , MonadResource m
     )
  => (Text -> m ())
  -> m Assets
load update' = do
  is <- Resource.createInternalState
  aKey <- Resource.register $ Resource.closeInternalState is
  let update = lift . update'
  flip Resource.runInternalState is do
    update "Loading sounds"
    (_soundDeviceKey, soundDevice) <- SoundDevice.allocate
    (_soundsKey, !aSounds) <- SoundSource.allocateCollectionWith
      (Opus.load soundDevice)
      Sound.configs
    SoundSource.play (Sound.bg aSounds)
    -- let
    --   update msg = do
    --     SoundSource.play (Sound.detect sounds)
    --     update' msg

    withPools \pools -> do
      update "Loading textures"
      -- Batch 1
      fonts <- traverse (Font.allocate pools) GameFont.configs
      textures <- traverse (Ktx2.load pools) Textures.sources

      let
        aFonts = fmap Font.container fonts
        aTextures = CombinedTextures.Collection
          { textures = textures
          , fonts    = fmap Font.texture fonts
          }

      -- Batch 2
      update "Loading cubes"
      aCubeMaps <- traverse (Ktx2.load pools) CubeMap.sources

      -- Batch 3
      aModels <- loadModels pools update
      aMaterials <- collectMaterials update aModels

      update "Assets loaded"
      pure Assets{..}

loadModels
  :: ( MonadResource m
     , MonadVulkan env m
     , HasLogFunc env
     )
  => Queues Vk.CommandPool
  -> (Text -> m ())
  -> m GameModel.Collection
loadModels pools updateProgress = do
  updateProgress "Loading models"

  bbWire <- Model.createStagedL (Just "bbWire") pools Cube.bbWireColored Nothing
  Model.registerIndexed_ bbWire

  quadUV <- Model.createStagedL (Just "quadUV") pools (Quad.toVertices Quad.texturedQuad) Nothing
  Model.registerIndexed_ quadUV

  icosphere1 <- allocateSphere pools 1
  icosphere4 <- allocateSphere pools 4

  zeroTransform <-
    Buffer.createStaged
      (Just "zeroTransform")
      pools
      Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
      1
      [mempty]
  Buffer.register zeroTransform

  updateProgress "data/models/craft_racer.weld3"
  (_craftRacerKey, craftRacer) <- Mesh.loadIndexed pools "data/models/craft_racer.weld3"

  pure GameModel.Collection{..}

collectMaterials
  :: Monad m
  => (Text -> m ())
  -> GameModel.Collection
  -> m MaterialCollection
collectMaterials updateProgress loadedModels = do
  updateProgress "Collecting materials..."

  pure $
    Collect.sceneMaterials
      loadedModels
      (CombinedTextures.textures Combined.indices)
      Nothing

allocateSphere
  :: ( MonadVulkan env m
     , Resource.MonadResource m
     )
  => Queues Vk.CommandPool
  -> Natural
  -> m (LitColored.Model 'Buffer.Staged)
allocateSphere pools details = do
  model <- Model.createStaged
    (Just $ "Sphere/" <> fromString (show details))
    pools
    pv
    av
    iv
  Model.registerIndexed_ model
  pure model
  where
    (pv, av, iv) =
      Icosphere.generateIndexed
        details
        mkInitialAttrs
        mkMidpointAttrs
        mkVertices

    mkInitialAttrs _pos = ()

    mkMidpointAttrs (_scale :: Float) _midPos _attr1 _attr2 = ()
      -- Vec3.lerp
      --   (scale * scale)
      --   (hash33 midPos)
      --   (Vec3.lerp 0.5 attr1 attr2)

    mkVertices points _faces = do
      (rawPos, ()) <- points
      let normPos = Vec3.normalize rawPos

      pure
        ( Vec3.Packed normPos
        , LitColored.VertexAttrs
            { vaBaseColor         = Vec4.fromVec3 (normPos * v05 + v05) 1
            , vaEmissiveColor     = 0.1
            , vaMetallicRoughness = vec2 0.5 0.5
            , vaNormal            = Vec3.Packed normPos
            }
        )

    v05 = vec3 0.5 0.5 0.5
