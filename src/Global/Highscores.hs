{-# OPTIONS -fno-warn-orphans #-}

{-# LANGUAGE TemplateHaskell #-}

module Global.Highscores
  ( Boards(..)

  , getPlayer
  , submit
  , fetch

  , Player(..)
  ) where

import RIO

import Codec.Compression.Zstd qualified as Zstd
import Data.FileEmbed (embedFileIfExists)
import Data.UUID (UUID)
import Data.UUID.V4 qualified as UUID
import Data.Yaml ((.:))
import Data.Yaml qualified as Yaml
import Highscores qualified
import Network.HTTP.Client (Manager)
import Network.HTTP.Client.TLS qualified as TLS
import RIO.Directory (XdgDirectory(..), createDirectoryIfMissing, doesFileExist, getXdgDirectory)
import RIO.FilePath ((</>))

data Boards = Boards
  { speeds :: Highscores.ScoreBoard
  , times  :: Highscores.ScoreBoard
  , burns  :: Highscores.ScoreBoard
  } deriving (Show, Generic)

fetch
  :: ( MonadUnliftIO m
     , MonadReader env m
     , HasLogFunc env
     )
  => Text
  -> m (Maybe Boards)
fetch playerName =
  withHighscores playerName \Player{playerId} Config{..} manager -> do
    topSpeeds <- Highscores.fetch topSpeedApp manager playerId Highscores.Desc
    topTimes  <- Highscores.fetch topTimeApp manager playerId Highscores.Asc
    topBurns  <- Highscores.fetch topBurnApp manager playerId Highscores.Asc

    speeds <- either throwString pure topSpeeds
    times  <- either throwString pure topTimes
    burns  <- either throwString pure topBurns
    pure Boards{..}

submit
  :: ( MonadUnliftIO m
     , MonadReader env m
     , HasLogFunc env
     )
  => Text
  -> Integer
  -> Maybe Integer
  -> Maybe Integer
  -> m Bool
submit setPlayerName topSpeed goalTime burnTime =
  fmap isJust $
    withHighscores setPlayerName \Player{..} Config{..} manager -> do
      Highscores.submit topSpeedApp manager Highscores.Submit
        { submitPlayerId   = playerId
        , submitPlayerName = playerName
        , submitScore      = topSpeed
        , submitOrder      = Highscores.Desc
        }

      for_ goalTime \topTime ->
        Highscores.submit topTimeApp manager Highscores.Submit
          { submitPlayerId   = playerId
          , submitPlayerName = playerName
          , submitScore      = topTime
          , submitOrder      = Highscores.Asc
          }

      for_ burnTime \topBurn ->
        Highscores.submit topBurnApp manager Highscores.Submit
          { submitPlayerId   = playerId
          , submitPlayerName = playerName
          , submitScore      = topBurn
          , submitOrder      = Highscores.Asc
          }

      logDebug "Highscores submitted"

withHighscores
  :: ( MonadUnliftIO m
     , MonadReader env m
     , HasLogFunc env
     )
  => Text
  -> (Player -> Config -> Manager -> m a)
  -> m (Maybe a)
withHighscores setPlayerName action =
  handleAny (fmap (const Nothing) . logError . displayShow) $
    case scoresConfig of
      Left err -> do
        logError $ "Highscore error: " <> fromString err
        pure Nothing
      Right config -> do
        manager <- liftIO TLS.getGlobalManager
        player <- getPlayer setPlayerName
        fmap Just $
          action player config manager

getPlayer :: MonadIO m => Text -> m Player
getPlayer name = do
  gameDir <- getXdgDirectory XdgConfig "ld49-258844"
  createDirectoryIfMissing True gameDir

  let playerFile = gameDir </> "player.yaml"
  playerExists <- doesFileExist playerFile

  if playerExists then
    liftIO (Yaml.decodeFileEither playerFile) >>= \case
      Left _err ->
        newPlayer playerFile name
      Right player -> do
        when (playerName player /= name) $
          liftIO . Yaml.encodeFile playerFile $ player
            { playerName = name
            }
        pure player
  else
    newPlayer playerFile name

newPlayer :: MonadIO m => FilePath -> Text -> m Player
newPlayer playerFile playerName = liftIO do
  playerId <- UUID.nextRandom
  let player = Player{..}
  Yaml.encodeFile playerFile player
  pure player

data Player = Player
  { playerId   :: UUID
  , playerName :: Text
  } deriving (Show, Generic)

instance Yaml.FromJSON Player
instance Yaml.ToJSON Player

data Config = Config
  { topSpeedApp :: Highscores.App
  , topTimeApp  :: Highscores.App
  , topBurnApp  :: Highscores.App
  }

instance Yaml.FromJSON Config where
  parseJSON = Yaml.withObject "Config" \o -> do
    topSpeedApp <- o .: "top-speed"
    topTimeApp <- o .: "top-time"
    topBurnApp <- o .: "top-burn"
    pure Config{..}

instance Yaml.FromJSON Highscores.App where
  parseJSON =  Yaml.withObject "App" \o -> do
    appId <- o .: "id"
    appSecret <- o .: "secret"
    pure Highscores.App{..}

scoresConfig :: Either String Config
scoresConfig = do
  zstd <- maybe (Left "no config embedded") pure getZstd
  case Zstd.decompress zstd of
    Zstd.Decompress bytes ->
      case Yaml.decodeEither' bytes of
        Left err ->
          Left $ show err
        Right config ->
          Right config
    err ->
      Left $ show err
  where
    getZstd =
      $(embedFileIfExists "secrets/highscores.yaml.zst")
