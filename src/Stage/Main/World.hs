{-# LANGUAGE QuasiQuotes #-}

module Stage.Main.World
  ( module Stage.Main.World
  , module RE
  ) where

import RIO.Local

import Apecs.STM.Prelude qualified as Apecs
import Control.Monad.Trans.Resource (ResourceT)
import Engine.Types qualified as Engine
import Engine.Vulkan.Types (MonadVulkan)
import Engine.Worker qualified as Worker
import GHC.Clock (getMonotonicTimeNSec)
import Resource.Buffer qualified as Buffer
import Resource.Model qualified as Model
import RIO.Vector.Storable qualified as Storable
import UnliftIO.Concurrent (forkIO, killThread)
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk

import Stage.Main.World.Body qualified as Body
import Stage.Main.World.Racer qualified as Racer
import Stage.Main.World.Type as RE (World, Time(..), Rand(..))
import Stage.Main.World.Wire qualified as Wire

numBodies :: Int
numBodies = 200

offset :: Vec3
offset = vec3 50 0 0

goalPos :: Vec3
goalPos = vec3 4250 0 0

goalRadius :: Float
goalRadius = 100

goalGravmass :: Float
goalGravmass = 2000

-- | Space scale to fit kilometers into engine depth range
pattern SCALE :: (Eq a, Floating a) => a
pattern SCALE = 0.00390625 -- 1/256

type Simulation = Simulation_ World

data Simulation_ world = Simulation
  { sWorker :: ThreadId
  , sOutput :: Worker.Var ()
  , sActive :: TVar Bool
  , sWorld  :: world
  }

instance Worker.HasOutput (Simulation_ world) where
  type GetOutput (Simulation_ world) = ()
  getOutput Simulation{..} = sOutput

spawnSimulation
  :: MonadUnliftIO m
  => Int
  -> Bool
  -> m world
  -> (world -> m ())
  -> ResourceT m (Simulation_ world)
spawnSimulation interval startActive initAction stepAction = do
  sActive <- newTVarIO startActive
  sOutput <- Worker.newVar ()

  sWorld <- lift initAction

  sWorker <- forkIO . lift $ do
    startClock <- liftIO getMonotonicTimeNSec
    step sActive sWorld sOutput startClock

  _key <- Resource.register $ killThread sWorker

  pure Simulation{..}
  where
    step activeVar world output prevClock = do
      active <- readTVarIO activeVar
      when active do
        stepAction world
        Worker.updateOutput output Just

      stepClock <- liftIO getMonotonicTimeNSec
      let
        stepTime = (stepClock - prevClock) `div` 1000
        sleepTime = interval - fromIntegral stepTime
      threadDelay sleepTime

      -- sleepClock <- liftIO getMonotonicTimeNSec
      -- let sleepTimeActual = fromIntegral (sleepClock - stepClock) `div` 1000
      -- traceShowM
      --   ( interval
      --   , stepTime

      --   , sleepTime
      --   , sleepTimeActual
      --   , sleepTimeActual - sleepTime
      --   )

      nextClock <- liftIO getMonotonicTimeNSec
      step activeVar world output nextClock

{-

type DT = Quantity [si| s |] Double

data UIProbe = UIProbe
  { uiPlayerOrientation :: Worker.Var (Maybe (Orbiter.Player, Orbiter.Orientation, Maybe Orbiter.Thrust))
  , uiPlayerOrbit       :: Worker.Var (Maybe Orbiter)
  , uiTarget            :: Worker.Var (Maybe (Orbiter.Target, Orbiter))
  , uiMessageLog        :: [Worker.Var (Float, Vec4, Text)]
  }

mkTrajectory :: Orbit.Orbit Double -> Orbiter.Trajectory
mkTrajectory orbit =
  Orbiter.Trajectory do
    (a, b) <- zip vertices (drop 1 $ cycle vertices)
    [a, b]
  where
    velPeriapsis =
      velocityKmS . stateVectorsAtTrueAnomaly orbit $
        quOf 0 [si| rad |]

    velApoapsis =
      velocityKmS . stateVectorsAtTrueAnomaly orbit $
        turn |/| 2

    segments =
      case Orbit.period orbit of
        Nothing ->
          128 -- XXX: give a bit of leeway for non-elliptic orbits
        Just seconds ->
          max 64 . min 256 $ ceiling (seconds `numIn` [si| s |]) `div` 180

    segment = τ / fromInteger segments
    vertices = do
      i <- [0 .. segments]
      let
        atSegment =
          stateVectorsAtTrueAnomaly orbit $
            quOf (fromInteger i * segment) [si| rad |]

        position =
          toPosition $ StateVectors.position atSegment

        color =
          case Orbit.periapsisSpecifier orbit of
            Orbit.Circular ->
              circColor
            _ ->
              Vec4.lerp velocityNorm slowColor fastColor
              where
                velocityNorm = double2Float . max 0 . min 1 $
                  (velocityKmS atSegment - velApoapsis) /
                  (velPeriapsis - velApoapsis)

        slowColor = vec4 1 0 0 0.1
        fastColor = vec4 0 1 0 0.9
        circColor = vec4 0 0 1 0.9

      pure Model.Vertex
        { vPosition = position
        , vAttrs    = color
        }

velocityKmS :: StateVectors.StateVectors Double -> Double
velocityKmS =
  Linear.norm . fmap (`numIn` [si| km/s |]) . StateVectors.velocity

toPosition :: Linear.V3 (Distance Double) -> Vec3.Packed
toPosition v3 = Vec3.Packed $ toVec3 v3km Vec3.^* SCALE
  where
    v3km = fmap (double2Float . (`numIn` [si| km |])) v3

orbiterScale :: Transform
orbiterScale = Transform.scale (SCALE * 6371 / 32)

toVec3 :: Linear.V3 Float -> Vec3
toVec3 (Linear.V3 x y z) = vec3 x (-z) y

-- bodyRadius :: Quantity [si| km |] Double
-- bodyRadius = quOf 6371 [si| km |]

-- karmanLine :: Quantity [si| km |] Double
-- karmanLine = quOf 100 [si| km |]

-- bodyMass :: Quantity [si| kg |] Double
-- bodyMass = quOf 5.9722e24 [si| kg |]

-- bodyMu :: Quantity [si| m^3 s^-2 |] Double
-- bodyMu = bodyMass |*| gravity_G
-}

type Observer = Worker.ObserverIO WorldBuffers

data WorldBuffers = WorldBuffers
  { bodies     :: TransformBuffer
  , racers     :: TransformBuffer
  , debugWires :: Wire.Model
  }

type TransformBuffer = Buffer.Allocated 'Buffer.Coherent Transform

newObserver :: ResourceT (Engine.StageRIO st) Observer
newObserver = do
  bodies <- new "bodies" 256
  racers <- new "racers" numRacers

  emptyWires <- Model.createCoherentEmpty (Just "wires") numWires
  debugWires <- Model.updateCoherent mempty emptyWires
  Model.registerIndexed_ debugWires

  Worker.newObserverIO WorldBuffers{..}

  where
    numRacers = 1

    numWires =
      65536 -- plz no overflow...
      -- 2 *       -- 2 vertices
      -- 60 * 5 *  -- 5 minutes
      -- numRacers -- per racer

    new label size =
      fmap snd $
        Buffer.allocateCoherent
          (Just label)
          Vk.BUFFER_USAGE_VERTEX_BUFFER_BIT
          size
          mempty

collect
  :: forall components w m a
  .  ( Apecs.Get w m components
     , Apecs.Members w m components
     )
  => (components -> a)
  -> Apecs.SystemT w m [a]
collect f = Apecs.cfold (\acc comps -> f comps : acc) []

collectMaybe
  :: forall components w m a
  .  ( Apecs.Get w m components
     , Apecs.Members w m components
     )
  => (components -> Maybe a)
  -> Apecs.SystemT w m [a]
collectMaybe f = Apecs.cfold (\acc -> maybe acc (: acc) . f) []

{-# INLINE observe #-}
observe :: MonadVulkan env m => Simulation -> Observer -> m ()
observe simP sim = do
  Worker.observeIO_ simP sim \WorldBuffers{..} () -> do
    (worldBodies, worldRacers, worldWires) <-
      atomically . Apecs.runWith (sWorld simP) $ (,,)
        <$> collect Body.inst
        <*> collect Racer.transform
        <*> collect (\(Wire.Wire vs) -> vs)

    WorldBuffers
      <$> Buffer.updateCoherentResize_ bodies (Storable.fromList worldBodies)
      <*> Buffer.updateCoherentResize_ racers (Storable.fromList worldRacers)
      <*> Model.updateCoherent (concat worldWires) debugWires

{-# INLINE readObserved #-}
readObserved :: MonadUnliftIO m => Observer -> m WorldBuffers
readObserved = Worker.readObservedIO
