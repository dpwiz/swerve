{-# LANGUAGE OverloadedLists #-}

module Stage.Main.Resource where

import RIO.Local

import Control.Monad.Trans.Resource (ResourceT)
import Engine.Camera qualified as Camera
import Engine.Stage.Component qualified as Stage
import Engine.Types (StageRIO)
import Engine.Types qualified as Engine
import Engine.Vulkan.Types (Queues)
import Engine.Worker qualified as Worker
import Render.Basic qualified as Basic
import Render.DescSets.Set0 qualified as Set0
import Render.DescSets.Sun qualified as Sun
import Render.ShadowMap.RenderPass qualified as ShadowPass
import Resource.Buffer qualified as Buffer
import Resource.Image qualified as Image
import Resource.Region qualified as Region
import UnliftIO.Resource qualified as Resource
import Vulkan.Core10 qualified as Vk

import Global.Render (RenderPasses, Pipelines)
import Global.Render qualified as Render
import Global.Resource.Assets (Assets)
import Global.Resource.Assets qualified as Assets
import Stage.Main.Scene qualified as Scene
import Stage.Main.Types (FrameResources(..), RunState(..))
import Stage.Main.UI qualified as UI
import Stage.Main.World qualified as World
import Stage.Main.World.System qualified as WorldSystem

component :: Assets -> Stage.Resources RenderPasses Pipelines RunState FrameResources
component assets = Stage.Resources
  { rInitialRS = initialRunState assets
  , rInitialRR = initialFrameResources assets
  }

initialRunState :: Assets -> StageRIO env (Resource.ReleaseKey, RunState)
initialRunState rsAssets = Region.run do
  rsProjectionP <- Camera.spawnPerspective

  screen <- lift Engine.askScreenVar

  rsCursorPos <- Worker.newVar 0
  rsCursorP <-
    Worker.spawnMerge2
      (\Vk.Extent2D{width, height} (WithVec2 windowX windowY) ->
          vec2
            (windowX - fromIntegral width / 2)
            (windowY - fromIntegral height / 2)
      )
      screen
      rsCursorPos

  screenBoxP <- Camera.trackOrthoPixelsCentered

  rsCameraP <- Scene.spawnCamera

  rsSceneP <- Worker.spawnMerge2 Scene.mkScene rsProjectionP rsCameraP

  ortho <- Camera.spawnOrthoPixelsCentered
  rsSceneUiP <- Worker.spawnMerge1 Scene.mkSceneUi ortho

  rsUIP <- UI.spawn rsAssets screenBoxP

  rsWorldSim <-
    WorldSystem.spawn
      rsAssets
      rsProjectionP
      rsCameraP
      rsSceneP
      rsUIP

  -- (updateShadowKey, rsUpdateShadowP) <- Worker.registered $ Worker.spawnMerge2 (\_env _sun -> ()) rsEnvP rsSunP
  rsUpdateShadowP <- Worker.newVar ()
  rsUpdateShadow <- Worker.newObserverIO ()

  let rsEvents = Nothing

  pure RunState{..}

initialFrameResources
  :: Assets
  -> Queues Vk.CommandPool
  -> RenderPasses
  -> Pipelines
  -> ResourceT (Engine.StageRIO RunState) FrameResources
initialFrameResources assets _pools passes pipelines = do
  (_lightsKey, lightsData) <- Buffer.allocateCoherent -- TODO: staged?
      (Just "lights")
      Vk.BUFFER_USAGE_UNIFORM_BUFFER_BIT
      0
      Scene.staticLights

  frScene <- Set0.allocate
    (Render.getSceneLayout pipelines)
    (Assets.aTextures assets)
    (Assets.aCubeMaps assets)
    (Just lightsData)
    [ Image.aiImageView . ShadowPass.smDepthImage $
        Basic.rpShadowPass (Render.rpBasic passes)
    ]
    Nothing

  frSceneUi <- Set0.allocate
    (Render.getSceneLayout pipelines)
    (Assets.aTextures assets)
    (Assets.aCubeMaps assets)
    Nothing
    mempty
    Nothing

  -- TODO: extract to Sun.allocateStatic
  (frSunDescs, frSunData) <- Sun.createSet0Ds $
    Basic.getSunLayout (Render.pBasic pipelines)
  Buffer.updateCoherent Scene.staticLights frSunData

  -- ui <- gets rsUI
  -- frUI <- UI.newObserver ui

  frWorld <- World.newObserver

  pure FrameResources{..}
