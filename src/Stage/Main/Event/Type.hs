module Stage.Main.Event.Type
  ( Event(..)
  ) where

import Stage.Main.Scene qualified as Scene
import Stage.Main.World.Racer.Event qualified as Racer

data Event
  = Camera Scene.CameraEvent
  | Pause
  | StepForward
  | Racer Racer.Event
  | Restart
