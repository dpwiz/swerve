module Stage.Main.Event.Key
  ( callback
  , keyHandler
  ) where

import RIO.Local

import Engine.Events.Sink (MonadSink, Sink(..))
import Engine.Window.Key (Key(..), KeyState(..))
import Engine.Window.Key qualified as Key
import UnliftIO.Resource (ReleaseKey)

import Stage.Main.Event.Type (Event)
import Stage.Main.Event.Type qualified as Event
import Stage.Main.Types (RunState(..))
import Stage.Main.World.Racer.Event qualified as Racer

callback :: MonadSink RunState m => Sink Event RunState -> m ReleaseKey
callback = Key.callback . keyHandler

keyHandler :: MonadSink RunState m => Sink Event RunState -> Key.Callback m
keyHandler (Sink signal) keyCode keyEvent@(_mods, state, key) =
  unless (state == KeyState'Repeating) do
    logDebug $ "Key event (" <> display keyCode <> "): " <> displayShow keyEvent
    case key of
      Key'Up ->
        signal . Event.Racer $ Racer.Burn pressed

      Key'Left ->
        signal . Event.Racer . Racer.Turn $
          if pressed then
            Just (-1)
          else
            Nothing

      Key'Right ->
        signal . Event.Racer . Racer.Turn $
          if pressed then
            Just 1
          else
            Nothing

      Key'W | pressed ->
        signal $ Event.Racer Racer.UseBoost

      Key'S | pressed ->
        signal $ Event.Racer Racer.UseShed

      Key'X | pressed ->
        signal $ Event.Racer Racer.UseJump

      Key'Escape | pressed ->
        signal Event.Restart

      Key'Pause | pressed ->
        signal Event.Pause

      Key'F12 | pressed ->
        signal Event.StepForward

      _ ->
        pure ()
  where
    pressed = state == KeyState'Pressed
