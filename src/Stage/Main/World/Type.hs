{-# LANGUAGE TemplateHaskell #-}

module Stage.Main.World.Type
  ( World
  , initWorld
  , StmWorld
  , CanSet

  , Time(..)
  , Rand(..)
  ) where

import Prelude
import Apecs.STM.Prelude

import Data.Monoid (Sum(..))
import System.Random (StdGen)

import Stage.Main.World.Body qualified as Body
import Stage.Main.World.Racer qualified as Racer
import Stage.Main.World.Wire qualified as Wire
import Stage.Main.World.Spark qualified as Spark

newtype Time = Time Float
  deriving (Eq, Ord, Num)
  deriving (Semigroup, Monoid) via (Sum Float)

instance Component Time where
  type Storage Time = Global Time

newtype Rand = Rand { randStdGen :: StdGen }

instance Component Rand where
  type Storage Rand = Unique Rand

makeWorld "World"
  [ ''Wire.Wire

  , ''Body.Body
  , ''Body.Goal

  , ''Racer.Racer
  , ''Racer.Forward
  , ''Racer.Backward
  , ''Racer.Crashed
  , ''Racer.ControlKB
  , ''Racer.Turn
  , ''Racer.Burn
  , ''Racer.Boost
  , ''Racer.Shed
  , ''Racer.Jump

  , ''Spark.Cluster

  , ''Time
  , ''Rand
  ]

type StmWorld = SystemT World STM

type CanSet components = Set World STM components
