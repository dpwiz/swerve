module Stage.Main.World.Racer.Event
  ( Event(..)
  ) where

import RIO

data Event
  = Turn (Maybe Float)
  | Burn Bool
  | UseBoost
  | UseShed
  | UseJump
  deriving (Eq, Ord, Show, Generic)
