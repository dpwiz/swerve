module Stage.Main.World.Spark
  ( Cluster(..)

  , Particle(..)
  ) where

import RIO.Local

import Apecs.STM.Prelude qualified as Apecs
-- import Geomancy.Vec3 qualified as Vec3
-- import Resource.Buffer qualified as Buffer
-- import Resource.Model qualified as Model
-- import Render.Unlit.Colored.Model qualified as UnlitColored

newtype Cluster = Cluster
  { particles :: [Particle]
  } deriving (Show)

instance Apecs.Component Cluster where
  type Storage Cluster = Apecs.Map Cluster

data Particle = Particle
  { position   :: Vec3
  , velocity   :: Vec3
  , colorsHead :: (Vec4, Vec4)
  , colorsTail :: (Vec4, Vec4)
  , time       :: Float
  , timeMax    :: Float
  } deriving (Show)
