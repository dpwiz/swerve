module Stage.Main.World.Racer
  ( Racer(..)
  , Vertex
  , Model
  , transform

  , Forward(..)
  , Backward(..)
  , Crashed(..)
  , NotCrashed

  , ControlKB(..)
  , Turn(..)
  , Burn(..)

  , Boost(..)
  , NotBoosted
  , maxBoostTimer

  , Shed(..)
  , NotShedding
  , maxShedTimer

  , Jump(..)
  , NotJumping
  , maxJumpTimer
  ) where

import RIO.Local

import Apecs.STM.Prelude qualified as Apecs
import Geomancy.Transform qualified as Transform
import Geomancy.Vec3 qualified as Vec3
import Render.Lit.Colored.Model qualified as LitColored
import Resource.Buffer qualified as Buffer
import Resource.Model qualified as Model
import Utils.Trajectory qualified as Trajectory

-- * Core component

data Racer = Racer
  { model       :: Model
  , position    :: Vec3
  , velocity    :: Vec3
  , direction   :: Float

  , boostCharge :: Float
  , shedCharge  :: Float
  , jumpCharge  :: Float
  }

instance Apecs.Component Racer where
  type Storage Racer = Apecs.Map Racer

type Vertex = Model.Vertex Vec3.Packed LitColored.VertexAttrs

type Model = Model.Indexed 'Buffer.Staged Vec3.Packed LitColored.VertexAttrs

transform :: (Racer, Maybe Jump) -> Transform
transform (Racer{position, direction}, mJump) =
  Transform.rotateZ (-direction) <>
  Transform.translateV shifted
  where
    shifted =
      case mJump of
        Nothing ->
          position
        Just Jump{jumpTimer} ->
          let
            alpha = jumpTimer / maxJumpTimer
            depth = 100 * id (sin $ pi * alpha)
          in
            position + vec3 0 0 depth

-- * Trajectories

newtype Forward = Forward [Trajectory.Segment]

instance Apecs.Component Forward where
  type Storage Forward = Apecs.Unique Forward

newtype Backward = Backward [Vec3]

instance Apecs.Component Backward where
  type Storage Backward = Apecs.Unique Backward

data Crashed = Crashed

instance Apecs.Component Crashed where
  type Storage Crashed = Apecs.Unique Crashed

type NotCrashed = Apecs.Not Crashed

-- * Controls

data ControlKB = ControlKB

instance Apecs.Component ControlKB where
  type Storage ControlKB = Apecs.Unique ControlKB

newtype Turn = Turn Float

instance Apecs.Component Turn where
  type Storage Turn = Apecs.Map Turn

newtype Burn = Burn Float

instance Apecs.Component Burn where
  type Storage Burn = Apecs.Map Burn

-- * Items

newtype Boost = Boost { boostTimer :: Float }

maxBoostTimer :: Float
maxBoostTimer = 4.0

instance Apecs.Component Boost where
  type Storage Boost = Apecs.Map Boost

type NotBoosted = Apecs.Not Boost

newtype Shed = Shed { shedTimer :: Float }

maxShedTimer :: Float
maxShedTimer = 2.0

instance Apecs.Component Shed where
  type Storage Shed = Apecs.Map Shed

type NotShedding = Apecs.Not Shed

newtype Jump = Jump { jumpTimer :: Float }

maxJumpTimer :: Float
maxJumpTimer = 2.0

instance Apecs.Component Jump where
  type Storage Jump = Apecs.Map Jump

type NotJumping = Apecs.Not Jump
