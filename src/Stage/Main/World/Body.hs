module Stage.Main.World.Body
  ( Body(..)
  , Vertex
  , Model

  , Goal(..)
  ) where

import RIO.Local

import Apecs.STM.Prelude qualified as Apecs
import Geomancy.Vec3 qualified as Vec3
import Resource.Buffer qualified as Buffer
import Resource.Model qualified as Model
import Render.Lit.Colored.Model qualified as LitColored

data Body = Body
  { model    :: Model
  , position :: Vec3
  , radius   :: Float
  , gravmass :: Float
  , inst     :: LitColored.InstanceAttrs
  }

instance Apecs.Component Body where
  type Storage Body = Apecs.Map Body

type Vertex = Model.Vertex Vec3.Packed LitColored.VertexAttrs

type Model = Model.Indexed 'Buffer.Staged Vec3.Packed LitColored.VertexAttrs

data Goal = Goal

instance Apecs.Component Goal where
  type Storage Goal = Apecs.Unique Goal
